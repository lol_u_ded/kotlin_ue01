//import java.awt.event.MouseAdapter
//import java.awt.event.MouseEvent
//import java.awt.event.MouseWheelEvent
//import javax.swing.JButton
//import kotlin.reflect.KProperty0
//import kotlin.reflect.KProperty1
//
////import java.awt.Color
////import kotlin.math.pow
////
//////
//////// NO IMPLICIT CONVERSIONS
//////val i = 1
//////val d = 1.0
//////val d2 : Double = (1).toDouble()
//////val d3 : Long = 1L
//////val l = 1L + 3 // Long + Int => Long
//////
//////
//////// Collections
//////
//////// Creates an immutable list
//////val names = listOf("Adelheid", "Franz", "Fritz")
//////// names.add("Sepp-Seraphin") // Error: Unresolved reference add
//////val newNames = names + "Sepp-Seraphin" // Very inefficient, creates new list and copies all elements!
//////
//////// Creates a mutable list
//////data class Robot(val str: String, val i : Int, val listOf: List<Any>)
//////
//////val robots = mutableListOf<Robot>()
//////robots.add(Robot("Pew Pew Guy", 3, listOf()))
//////robots += Robot("Tanky", 5, listOf()) // operator "plusAssign" forwards to method "add"
//////println(robots[0] == robots.get(0)) // writes "true" - get() and set() can be replaced by brackets
//////
//////// also has equivalent mutable version
//////val numbers = setOf(1, 2, 4, 8, 16)
//////
//////// also has equivalent mutable version
//////val colors = mapOf(
//////    "Blue" to Color.BLUE,
//////    "Red".to(Color.RED),
//////    Pair("Green", Color.GREEN)
//////)
//////// colors.add ... doesnt work
//////println(colors["Blue"] == colors.get("Blue")) // writes "true" - get() and set() can be replaced by brackets
//////
//////// Lambda
//////
//////fun funnyString(s: String) =
//////    s.trim().reversed().mapIndexed { i, ch -> if (i % 2 == 0) ch.uppercaseChar() else ch.lowercaseChar() }
//////
//////println(funnyString("Hallo Matthias"))
//////
//////// WHEN
//////fun hasPrefix(x: Any) : Boolean = when(x) {
//////    is String -> x.startsWith("prefix")
//////    else -> false
//////}
//////
//////// FOR LOOPS
//////for (i in 1..3) { // same as 1.rangeTo(3)
//////    println(i)
//////}
//////for (i in 1 until 3) { // same as 1.until(3)
//////    println(i)
//////}
//////for (i in 6 downTo 0 step 2) { // same as 6.downTo(0).step(2)
//////    println(i)
//////}
//////
//////// Function - PARAMETERS
//////
//////// Parameter types must be explicitly typed
//////fun half(x: Int): Double {
//////    return x / 2.0
//////}
//////
//////fun powerOf(
//////    base: Int,
//////    exponent: Int, // trailing comma possible
//////): Double {
//////    return base.toDouble().pow(exponent.toDouble())
//////}
//////
//////// Inheritance
//////
//////open class Animal {
//////    var hunger = 0
//////    var isSleepy = false
//////
//////    // Default value is written after the parameter name
//////    open fun run(amount: Int = 20, where: String = "Forest") {
//////        hunger = Math.min(100, hunger + amount)
//////    }
//////
//////    // Default value is written after the parameter name
//////    open fun eat(amount: Int = 10) {
//////        hunger = Math.max(0, hunger - amount)
//////    }
//////
//////}
//////
//////class SleepyDog : Animal() {
//////    // No default value is allowed.
//////// Same default value as in parent class is used
//////    override fun eat(amount: Int) {
//////        super.eat(amount+2)
//////// SleepyDog gets tired during eating
//////        isSleepy = true
//////    }
//////}
//////
//////val billy = SleepyDog()
//////
//////// 'where' uses default value
//////billy.run(20)
//////
//////// 'amount' uses default value
//////billy.run(where = "Dog Park")
//////
//////// no default values, but different order
//////billy.run(where = "Dog Park", amount = 15)
//////
//////billy.eat()
//////billy.eat(100)
//////
//////fun doIt(what : String) {
//////    println("I do $what!")
//////}
//////
//////// Kotlin has a "return Type in void" is Unit
//////var sd = doIt("kill vader")
//////println(sd.toString())
//////
//////class Person  private constructor(firstName: String = "Defaulty") {
//////    // Property initializers and init blocks effectively become part of the primary constructor
//////// and are executed top to bottom
//////// (1)
//////    val firstName = firstName // Property type inferred
//////    val lastName = "Allthesame Lastname"
//////
//////    // (2)
//////    init {
//////        println(firstName)
//////// println(createdAt) // Error: Variable createdAt must be initialized
//////    }
//////
//////    // (3)
//////    var lastChange = 0L
//////
//////    // (4)
//////    init {
//////// this.firstName = firstName.uppercase() // Error: val cannot be reassigned
//////        lastChange = System.currentTimeMillis()
//////    }
//////}
//////
//////
//////// Primary constructor parameters can be marked with "val" and "var"
//////// The automatically become properties (we will talk about properties in a moment)
//////class PersonWithKids constructor(
//////    val firstName: String = "Defaulty",
//////    val lastName: String = "Allthesame Lastname"
//////) {
//////    val children = mutableListOf<String>()
//////
//////    // Secondary constructor
//////    constructor(
//////        firstName: String,
//////        lastName: String,
//////        kids: Collection<String>
//////    ) : this(firstName, lastName) { // Every secondary constructor must call the primary constructor
//////        children.addAll(kids)
//////    }
//////}
//////
//////
//////// Mandatory override
//////
//////
//////open class Parent(val firstName: String) {
//////    // Calculated property to prevent backing field
//////    open val nameLength
//////        get() = firstName.length
//////
//////    open fun doIt() {
//////        println("Do it!")
//////    }
//////
//////}
//////
//////class Child(
//////    firstName: String,
//////    val lastName: String
//////) : Parent(firstName) {
//////    override val nameLength // : Int
//////        get() = super.nameLength + lastName.length
//////
//////    override fun doIt() {
//////        super.doIt()
//////        println("But childish!")
//////    }
//////}
////
////
////// Similar to sealed interfaces there exist sealed classes
////// Sealed classes are abstract and cannot be instanciated
////sealed interface Printable {
////    fun print()
////
////    // default implementations are possible
////    fun printToString() {
////        println(toString())
////    }
////
////    val prefix: String // abstract, has to be overriden;
////// could also have a default implementation
////// get() = "> "
////// Properties defined in interfaces have no backing field
////// Thus, getters and setter cannot reference 'field'
////
////    fun printToStringWithPrefix() {
////        println("$prefix ${toString()}")
////    }
////
////}
////
////sealed interface Named {
////    val name: String
////}
////
////
////abstract class  A : Named{
////    override val name: String
////        get() = "Test"
////}
////
////class Animal(
////    override val name: String, // we can also override properties in the primary constructor
////    val kind: String
////) : Printable, Named {
////    // Was abstract, had to be overriden
////    override val prefix: String = "Animal:"
////
////// Even more elegant: Override with a getter, so no backing field is created:
////// override val prefix: String
//////    get() = "Animal:"
////
////    override fun print() {
////        println("I am the animal $name")
////    }
////
////    override fun toString(): String {
////        return name
////    }
////
////}
////
////
////class Shop(override val name: String, val location: String) : Named
//
//
////class Game(val name: String) {
////
////    inner class Card(val card: String) {
////        override fun toString(): String {
////// Access outer class with this@OuterClassName if necessary
////            return "Card $card of game $name"
////        }
////    }
////
////    fun createCard(cardName: String) =
//////this.Card(cardName)
////        Card(cardName)
////
////}
////
////val risk = Game("Risk")
////val card = risk.Card("Madagascar")
////val card2 = risk.createCard("Alaska")
////// instances of an inner class always belong to an object
////// of the corresponding outer class
////// val card3 = Game.Card("Standalone Card")
////
////println(card)
////println(card2)
//
//object DefaultListener : MouseAdapter() {
//    override fun mouseClicked(e: MouseEvent?) {
//        println("Mouse clicked")
//    }
//
//    override fun mouseWheelMoved(e: MouseWheelEvent?) {
//        println("Wheeee!")
//    }
//
//}
//val button = JButton()
//button.addMouseListener(DefaultListener)
//
//while(true){
//
//}
//
//
//data class Person(
//    val firstName: String = "Betty",
//    val lastName: String = "Broccolini",
//    val children: List<String> = listOf(),
//    var age: Int = 18
//) {
//    fun print() {
//        println(this)
//    }
//
//    fun hasLongName() = firstName.length + lastName.length >= 20
//
//}
////
//val bettyBroccolini = Person()
//val bettyBroccoliniAsAny: Any = bettyBroccolini
//
//val personKClass = Person::class
//val personJavaClass = personKClass.java
//val personKClass2 = personJavaClass.kotlin
//val personKClass3 = bettyBroccolini::class
//val personKClass4 = bettyBroccoliniAsAny::class // dynamically bound to Person, not Any
//
////val printPerson1 = Person::print // "this" is the first parameter
//////val printPerson2: KFunction1<Person, Unit> = Person::print
////val printPerson3: (Person) -> Unit = Person::print
////printPerson1(bettyBroccolini)
//////printPerson2(bettyBroccolini)
////printPerson3(bettyBroccolini)
////
////println(printPerson1.parameters)
////
////val printBetty1: KFunction0<Unit> = bettyBroccolini::print
////
////val printBetty2: () -> Unit = bettyBroccolini::print
////
//////printBetty1()
////printBetty2()
////
////val firstNameProp1: KProperty1<Person, String> = Person::firstName
////val firstNameProp2: KProperty0<String> = bettyBroccolini::firstName
////println(firstNameProp1.name)
////println(firstNameProp1.isOpen)
////println(firstNameProp1(bettyBroccolini)) // Betty
////println(firstNameProp1.get(bettyBroccolini)) // Betty
////println(firstNameProp1.call(bettyBroccolini)) // Betty
//
//
//var value = null
//
//val intList = value as? List<Int> ?: throw IllegalArgumentException("list expected")
//
////Generics baby!
//
//fun <T>printElems(list : MutableList<out T>) {
//    for (e in list) {
//        println(e)
//    }
//}
//
//fun <T, F : T> copyTo(from: MutableList<out F>, to: MutableList <in T>) {
//    for(f in from) {
//        to.add(f);
//    }
//}
//
//val mlist = mutableListOf<String>("Hallo", "Kotlin")
//val plist = mutableListOf<String>("ist", "Spast")
//copyTo(plist, mlist)
//printElems(mlist)
//
//interface Comparable<in T> {
//    operator fun compareTo(other: T): Int
//}
//
//abstract class Person(private val name: String) : Comparable<Person> {
//    override fun compareTo(other: Person): Int = 1
//}
//
//
//data class Student(val name: String) : Person(name)
//data class Professor(val name: String) : Person(name)
//
//
//abstract class FList<out E> {
//    abstract val isEmpty: Boolean
//}
//
//object Nil : FList<Nothing>() {
//    override val isEmpty: Boolean = true
//}
//
//data class Cons<out E>(val head: E, val tail: FList<E>) : FList<E>() {
//    override val isEmpty: Boolean = false
//}
//
//val action = fun (p: Person)= println(p)
//
//val x = StringBuilder()
//x.append(2).toString()
//
//val arr = IntArray(2)
//arr[0] = 2
//arr[1] = 65
//
//
//for(n : Int in 1..3){
//    print(n)
//}