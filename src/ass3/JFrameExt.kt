import java.awt.BorderLayout
import java.awt.Color
import java.awt.Container
import java.awt.FlowLayout
import java.awt.event.ActionListener
import javax.swing.*

fun frame(title: String, x: Int, y: Int, init: JFrameExt.() -> Unit) : JFrameExt {
    val frame = JFrameExt(title)
    frame.setLocation(x, y)
    frame.init()
    return frame
}
class JFrameExt(title: String) : JFrame(title) {

    fun open() {
        pack()
        isVisible = true
    }

    fun close() {
        isVisible = false
    }

    fun panel(): JPanel {
        return JPanel()
    }

    fun button(label: String): JButton {
        return JButton(label)
    }

    fun field(width: Int): JTextField {
        return JTextField(width)
    }

    fun label(text: String): JLabel {
        return JLabel(text)
    }

    fun menuBar(init: JMenuBar.() -> Unit) {
        jMenuBar = JMenuBar()
        jMenuBar.init()
    }


    fun JMenuBar.menu(name: String, init: JMenu.() -> Unit) {
        val menu = JMenu(name)
        this.add(menu)
        menu.init()
    }

    fun JMenu.menu(name: String, init: JMenu.() -> Unit) {
        val menu = JMenu(name)
        this.add(menu)
        menu.init()
    }

    fun JMenu.item(name: String) : JMenuItem {
        val item = JMenuItem(name)
        this.add(item)
        return item
    }

    infix fun JMenuItem.onEvent(handler: () -> Unit) : JMenuItem{
        this.addActionListener { handler() }
        return this
    }

    infix fun JTextField.onEvent(handler: () -> Unit) : JTextField{
        this.addActionListener { handler() }
        return this
    }

    operator fun JMenuItem.plus(handler: () -> Unit) : JMenuItem {
        addActionListener{ handler() }
        return this
    }


    operator fun <A : AbstractButton> A.plus(action: ActionListener): A {
        addActionListener(action)
        return this
    }

    infix fun AbstractButton.onEvent(action: () -> Unit) : AbstractButton{
        addActionListener{action()}
        return this
    }

    fun content(init: Container.() -> Unit) : Container {
        contentPane.init()
        return  contentPane
    }

    fun borderLayout(init: BorderLayoutBuilder.() -> Unit) : JPanel {
        val panel = BorderLayoutBuilder()
        panel.layout = BorderLayout()
        add(panel)
        panel.init()
        return panel
    }

    fun flowLayout(i: Int = 0, init: FlowLayoutBuilder.() -> Unit = {}): JPanel {
        val panel = JPanel()
        panel.layout = FlowLayout(i)
        FlowLayoutBuilder(panel).init()
        return panel
    }

    class FlowLayoutBuilder(panel: JPanel) : BorderBuilder(panel) {
        fun <C : JComponent> comp(jComp: C, init: C.() -> Unit = {}): C {
            panel.add(jComp)
            jComp.init()
            return jComp
        }
    }

    abstract class BorderBuilder(val panel: JPanel) {
        fun border(){
           panel.border = BorderFactory.createEtchedBorder(Color.BLUE, Color.PINK)
        }
    }

    class BorderLayoutBuilder : JPanel()  {
        private fun <C : JComponent> comp (pos: String, jComp: C, init: C.() -> Unit = {}) : C {
            add(jComp, pos)
            jComp.init()
            return jComp
        }

        fun <C: JComponent> north(jComp : C, init : C.() -> Unit = {}) : JComponent {
            return comp(BorderLayout.NORTH, jComp, init)
        }

        fun <C: JComponent> east(jComp : C, init : C.() -> Unit = {}) : JComponent {
            return comp(BorderLayout.EAST, jComp, init)
        }

        fun <C: JComponent> south(jComp : C, init : C.() -> Unit = {}) : JComponent {
            return comp(BorderLayout.SOUTH, jComp, init)
        }

        fun <C: JComponent> west(jComp : C, init : C.() -> Unit = {}) : JComponent {
            return comp(BorderLayout.WEST, jComp, init)
        }

        fun <C: JComponent> center(jComp : C, init : C.() -> Unit = {}) : JComponent {
            return comp(BorderLayout.CENTER, jComp, init)
        }
    }
}




fun main() {
    val frame = frame(  "Conversion",  200, 200)  {
        defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE


        val celsius = field(6)
        val fahrenh = field(6)
        val message = label(" ")

        menuBar {
            menu("File") {
                item("Exit") onEvent {
                    println("Exit with 0")
                } onEvent  {
                    System.exit(0)

                }
                menu("Settings"){
                    item("Dark mode")onEvent {
                        println("Now dark mode")
                    } onEvent  {
                        println("Yes, dark mode")
                    }
                }

            }
        }

        content {
            borderLayout {
                north(flowLayout(1) {
                    border()
                    comp(button("Reset")) onEvent {
                        celsius.text = "0"
                        fahrenh.text = "32"
                        message.text = "${celsius.text} C = ${fahrenh.text} F"
                    }
                })
                center(flowLayout {
                    border()
                    comp(celsius) { text = "0" } onEvent {
                        val c = celsius.text.filter { it.isDigit() || it == '-' }.toInt()
                        val f = c * 9 / 5 + 32
                        fahrenh.text = f.toString()
                        message.text = "${celsius.text} C = ${fahrenh.text} F"
                    }
                    comp(JLabel("Celsius = "))
                    comp(fahrenh) { text = "32" } onEvent {
                        val f = fahrenh.text.filter { it.isDigit() || it == '-' }.toInt()
                        val c = (f - 32) * 5 / 9
                        celsius.text = c.toString()
                        message.text = "${celsius.text} C = ${fahrenh.text} F"
                    }
                    comp(JLabel("Fahrenheit"))
                })
                south(message) {
                    border
                    text = "${celsius.text} C = ${fahrenh.text} F"
                }
            }
        }
    }
    frame.open()
}


