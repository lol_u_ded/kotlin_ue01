package ass1

import java.io.File

// Row 1 Element interface
sealed interface Element

//------------------------------------------------------------

// Row 2 Element 1
sealed interface TextElement : Element{
    val text: String
}

// Row 2 Element 2
sealed interface TaggedElement : Element {
    val tag: String
    val openTag: String
        get() = "<$tag>"
    val closeTag: String
        get() = "</$tag>"
}

//------------------------------------------------------------

// Row 3 Element 1
sealed interface ContainerElement : TaggedElement {
    val elements: List<Element>
}

// Row 3 Element 2
sealed interface TaggedTextElement : TaggedElement, TextElement

//------------------------------------------------------------

// Row 4 Element 1
data class Div(override val elements: List<Element>) : ContainerElement {
    override val tag: String
        get() = "div"

    constructor(vararg elements: Element) : this(elements.toList())
}

// Row 4 Element 2
data class ListItem(override val elements: List<Element>) : ContainerElement {
    override val tag: String
        get() = "li"

    constructor(vararg elements: Element) : this(elements.toList())
}

// Row 4 Element 3
data class HTMLList(val ordered: Boolean, override val elements: List<ListItem>) : ContainerElement {
    override val tag: String
        get() = if (ordered) "ol" else "ul"

    constructor( ordered: Boolean, vararg elements: ListItem) : this(ordered,elements.toList())
}

// Row 4 Element 4
data class Heading(override val text: String, val level: Int = 1) : TaggedTextElement {
    override var tag: String
        get() = "h$level"
        set(value) {}

    init {
        if (level !in 1..6) {
            error("level is not in range 1-6: level=$level")
        }
    }
}

// Row 4 Element 5
data class Paragraph(override val text: String) : TaggedTextElement {
    override val tag: String
        get() = "p"
}

// Row 4 Element 6
data class Text(override val text: String) : TextElement

//------------------------------------------------------------

// Row 5 Element 1

fun String.h(level: Int): Heading = Heading(this, level)
fun String.h1(): Heading = Heading(this, 1)
fun String.h2(): Heading = Heading(this, 2)
fun String.h3(): Heading = Heading(this, 3)
fun String.h4(): Heading = Heading(this, 4)
fun String.h5(): Heading = Heading(this, 5)
fun String.h6(): Heading = Heading(this, 6)

fun String.p(): Paragraph = Paragraph(this)
fun String.text(): Text = Text(this)

fun String.indentEachLine(depth: Int = 2, symbol: String = " ") =
    this.split("\n").joinToString("\n") { symbol.repeat(depth) + it }

fun main() {
    val page = Page(
        "My Page",
        "Welcome to the Kotlin course".h1(),
        Div(
            "Kotlin is".p(),
            HTMLList(
                true,
                ListItem("General-purpose programming language".h3(),
                    HTMLList(false,
                        ListItem("Backend, Mobile, Stand-Alone, Web, ...".text())
                    )
                ),
                ListItem(
                    "Modern, multi-paradigm".h3(),
                    HTMLList(
                        false,
                        ListItem("Object-oriented, functional programming (functions as first-class citizens, …), etc.".text()),
                        ListItem("Statically typed but automatically inferred types".text())
                    )
                ),
                ListItem(
                    "Emphasis on conciseness / expressiveness / practicality".h3(),
                    HTMLList(
                        false,
                        ListItem("Goodbye Java boilerplate code (getter methods, setter methods, final, etc.)".text()),
                        ListItem("Common tasks should be short and easy".text()),
                        ListItem("Mistakes should be caught as early as possible".text()),
                        ListItem("But no cryptic operators as in Scala".text())
                    )
                ),
                ListItem(
                    "100% interoperable with Java".h3(),
                    HTMLList(
                        false,
                        ListItem("You have a Java project? Make it a Java/Kotlin project in minutes with 100% interop".text()),
                        ListItem("Kotlin-to-Java as well as Java-to-Kotlin calls".text()),
                        ListItem("For example, Kotlin reuses Java’s existing standard library (ArrayList, etc.) and extends it with extension functions (opposed to, e.g., Scala that uses its own list implementations)".text())
                    )
                ),
            )
        )
    )
    val htmlRendered= HTMLRenderer.render(page)
    println(htmlRendered)
    File("homework.html").writeText(htmlRendered)
}

// Row 5 Element 2
data class Page(val title: String, val elements: List<Element>) {

    constructor( title: String,vararg elements: Element) : this( title, elements.asList())
}


// Row 5 Element 3
object HTMLRenderer {
    private var depth = 4

    private fun render(ele: Element): String =
        when (ele) {
            is ContainerElement -> {                                    // Div, ListItem, HTMLList
                val builder = StringBuilder()
                builder.append(ele.openTag.indentEachLine(depth)).append("\n")
                depth += 2
                for (i in ele.elements) {
                    builder.append(render(i)).append("\n")
                }
                depth -= 2
                builder.append(ele.closeTag.indentEachLine(depth))
                builder.toString()
            }
            is TaggedTextElement -> {                                   //  Heading, Paragraph
                "${ele.openTag}${ele.text}${ele.closeTag}".indentEachLine(depth)
            }
            is TextElement -> {                                         // Text
                ele.text.indentEachLine(depth)
            }
        }

    fun render(p: Page): String {
        val builder = StringBuilder()
        builder.append("<html>\n").append("  <head>\n")
        builder.append("    <title>").append(p.title).append("</title>\n")
        builder.append("  </head>\n")
        builder.append("  <body>\n")
        for(i in p.elements){
            builder.append(render(i)).append("\n")
        }

        builder.append("  </body>\n").append("<html>")
        return builder.toString()
    }
}