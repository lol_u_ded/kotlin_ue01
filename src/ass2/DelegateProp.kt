package ass2

import java.io.File
import kotlin.reflect.KProperty


data class Student(
    val dna : String
){
    var fieldOfStudy : String?  by StringFileStorage()
    var name: String?  by StringFileStorage()
    var stressLevel : Int?  by IntFileStorage()

    override fun toString(): String{
        return "$dna - $name - $fieldOfStudy - $stressLevel"
    }
}

private fun propertyFileName(thisRef: Student, property: KProperty<*>)=
    "${thisRef.dna}_${property.name}.prop"


class  StringFileStorage{
    operator fun setValue(thisRef: Student, property: KProperty<*>, newValue : String?) {
        val f = File(propertyFileName(thisRef, property))
        if(f.exists()) {
            f.delete()
        }
        if(newValue != null){
            f.createNewFile()
            f.writeText(newValue)
        }
    }

    operator fun getValue(thisRef: Student, property: KProperty<*>) : String? {
        val f = File(propertyFileName(thisRef, property))
        if(f.exists()){
            val content = f.readText()
            return content.ifEmpty {
                null
            }
        }
        return null
    }
}

class IntFileStorage {
    operator fun setValue(thisRef: Student, property: KProperty<*>, newValue: Int?) {
        val f = File(propertyFileName(thisRef, property))
        if(f.exists()) {
            f.delete()
        }
        if(newValue != null){
            f.createNewFile()
            f.writeText(newValue.toString())
        }
    }
    operator fun getValue(thisRef: Student, property: KProperty<*>): Int? {
        val f = File(propertyFileName(thisRef, property))
        if(f.exists()) {
            return f.readText().toInt()
        }
        return null
    }

}

fun main(){
    val stressedStudent = Student("Nullpointer")
    val chilledStudent = Student("LOG4J")
    val lazyStudent = Student("YYX")
    println(stressedStudent)

    stressedStudent.name = "Matthias"
    stressedStudent.stressLevel = 9001
    stressedStudent. fieldOfStudy = "ComputerScience"

    chilledStudent.name = "Mark"
    chilledStudent.stressLevel = 1500
    chilledStudent. fieldOfStudy = "ComputerScienceMaster"

    lazyStudent.name = "Kevin"
    lazyStudent.stressLevel = -50
    lazyStudent. fieldOfStudy = "BWL"

    println(stressedStudent)
    println(chilledStudent)
    println(lazyStudent)

}
