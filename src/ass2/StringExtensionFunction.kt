package ass2

fun main(){

    val testString = "..... pythoN and kotlIn are programming languages you C4n learn without 2 much pain and 0rdinary suffEring......"

    // (1) reversed
    // this will reverse any string
    val reversedString = testString.reversed()
    println("reversedString: $reversedString")

    // (2) chunked
    // The string will split the Characters into lists of given size
    val chunkedString = testString.chunked(3)
    println("chunkedString: $chunkedString")

    // (3) dropWhile
    // This will drop from the beginning every Character that fulfills the
    // lambda expression until it reaches one that does not.
    val droppedFromBeginning = testString.dropWhile {  !it.isLetter() }
    println("droppedFromBeginning: $droppedFromBeginning")

    // (4) filter
    // This will filter out any char that fulfills the given setting
    val filteredWhitespaces = testString.filter { !it.isWhitespace() }
    println("filteredWhitespaces: $filteredWhitespaces")

    // (5) findAnyOf
    // finds the first occurrence of specified string
    val findAnyAnd = testString.findAnyOf(listOf("KOTLIN"), 15, true)
    println("findAnyAnd: $findAnyAnd")

    // (6) forEach
    // performs the action for each character in the given string
    print("forEachDo: ")
    testString.forEach { print(it.plus(1)) }
    println("")


    // (7) indexOfAny
    // it will find the index of the first occurrence of the given character in the String
    val idxOf2 = testString.indexOf('2')
    println("idxOf2 = $idxOf2")

    // (8) map
    // This will transform the Characters of the string into a list into the form specified
    val mapped  = testString.filter { it.isLetter() }.map{ it.lowercase() }         // filter applied: only letters
    println("mapped: $mapped")

    // (9) slice
    // will cut out a string from range x to y
    val sliced = testString.slice(79..82)
    println("sliced: [$sliced]")

    // (10) partition
    // splits the String into pair, the first one contains all chars that fulfill the specification
    val partitionedNumbers = testString.partition { it.isDigit() }.first
    val partitionedUppers = testString.partition { it.isUpperCase() }.first
    println("partitionedNumbers and Uppers: $partitionedNumbers is $partitionedUppers")




}