package ass2

import java.io.File

fun main(){

    println("-----------------------------------------\nday1: ")
    day1()
    println("-----------------------------------------\nday2: ")
    day2()
}


sealed interface PosInt {
    val horizontal: Int
    val depth: Int
}

// #2 Write and use at least one data class (for example to store your data)
// #4 Override and use at least one operator (either implemented in a class or as an extension method)
data class Position(override var horizontal: Int, override var depth: Int) : PosInt

fun day1() {
// The problem-file
    val content = "D:\\OneDrive\\Informatik\\Semester 5\\KV Kotlin\\homework\\src\\ass2\\input_1.txt".readLinesAsInt()
/*
Star 1
 */

    var prev = Int.MAX_VALUE                                        // holds the prev value
    var idx = 0                                                     // index of array
    var valIdx = 0                                                  // value number in array 0 to 2
    val data = Array(content.size+2) {Array(3) {0} }       // array to sort in values
    var solution = 0                                                // solution

    while( idx < content.size){

        // Array[index][valuePosition] = value
        data[idx][valIdx] = content[idx]
        data[idx+1][valIdx] = content[idx]
        data[idx+2][valIdx] = content[idx]

        when {
            valIdx < 2 -> {
                valIdx++
            }
            else -> {
                valIdx = 0
            }
        }
        idx++
    }

// #1 Write and use at least one lambda expression (for example to map each read line (String) to a suitable type (e.g., Int))
// Count the values that are bigger than the
// previous
    (2 until data.size).forEach { i ->
        val sum = data[i][0] + data[i][1] + data[i][2]
        when {
            prev == Int.MAX_VALUE -> {
                println("$sum (N/A - no previous sum)")
            }
            sum > prev -> {
                println("$sum (increased)")
                solution++
            }
            sum < prev -> {
                println("$sum (decreased)")
            }
            else -> {                               // sum == prev
                println("$sum (no change)")

            }
        }
        prev = sum
    }
    println("\nIn this example, there are $solution sums that are larger than the previous sum. ")
}

fun day2() {

    val plannedCourse = "D:\\OneDrive\\Informatik\\Semester 5\\KV Kotlin\\homework\\src\\ass2\\input_2.txt".readLines()

    val down = "down"
    val up = "up"
    val forward = "forward"
    val pos = Position(0,0)

    // #1 Write and use at least one lambda expression (for example to map each read line (String) to a suitable type (e.g., Int))
    plannedCourse.forEach{ s ->
        when {
            s.contains(forward) -> {
                val x = s.remove("$forward ").toInt()
                pos.horizontal += x
                println("forward $x adds $x to your horizontal position, a total of ${pos.horizontal}")
            }
            s.contains(up) -> {
                val x = s.remove("$up ").toInt()
                pos.depth -= x
                println("up $x decreases your depth by $x, resulting in a value of ${pos.depth}")
            }
            s.contains(down ) -> {
                val x = s.remove("$down ").toInt()
                pos.depth += x
                println("up $x adds $x to your depth, resulting in a value of ${pos.depth}")
            }
        }
    }

    println("\nAfter following these instructions, you will have a horizontal position of ${pos.horizontal} and a depth of ${pos.depth}. ")
}

// extensions
// #3 Write and use at least on extension method (for example to read the data from a File)
// #1 Write and use at least one lambda expression (for example to map each read line (String) to a suitable type (e.g., Int))
fun String.readLines(): List<String> = File(this).readLines()
fun String.readLinesAsInt() = this.readLines().map { it.toInt() }
fun String.remove(str: String) = this.replace(str, "")