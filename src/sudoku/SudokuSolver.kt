package sudoku

import java.io.File
import java.lang.StringBuilder
import kotlin.math.sqrt

class SudokuSolver (private val setOfSudoku : Boolean = false) {

    private val begin = System.currentTimeMillis()

    var identifiers : List<String> = mutableListOf()
    var solutions : HashMap<String, Array<IntArray>> = hashMapOf()

    fun solve(saveToFile: Boolean, pathProblem: String, pathSolution: String = "None"){
        if(setOfSudoku){
            val problemSet = fillExtended(pathProblem)
            problemSet.forEach { mainPro(it, null, saveToFile) }
        }else {
            if (pathSolution != "None") {
                mainPro(fill(pathProblem), pathSolution, saveToFile)
            } else {
                mainPro(fill(pathProblem), null, saveToFile)
            }
        }
    }

    private fun mainPro(sudokuBoard: Array<IntArray>, pathSol: String ? , saveToFile:  Boolean){

        val identifier = sudokuBoard.hashCode().toString()
        print(sudokuBoard, false)
        if(saveToFile){ toFile(sudokuBoard = sudokuBoard,false, identifier) }

        if (solver(sudokuBoard)) {
            print(sudokuBoard,  true)

            println("\nIt took ${System.currentTimeMillis() - begin} ms to solve that problem")
        } else {
            println("No solution was found")
        }

        val sudokuBoardSol = pathSol?.let { fill(it) }

        if (sudokuBoard.contentDeepEquals(sudokuBoardSol)) {
            println("\nSolution is correct")
        }

        if(saveToFile){
            toFile(sudokuBoard,true,identifier)

        }
        solutions[identifier] = sudokuBoard
        this.identifiers += identifier
    }

    // get initial status of Sudoku
    private fun fill(path: String): Array<IntArray> {
        val sudokuProblem = Array(9) { IntArray(9) }
        val content = path.readLines()
        val list = content.map { it.split(" ") }
        (0..8).forEach { r ->
            (0..8).forEach { c ->
                sudokuProblem[r][c] = list[r][c].toInt()
            }
        }
        return sudokuProblem
    }

    private fun fillExtended(path: String): Array<Array<IntArray>> {
        val sudokuProblemSet = Array(1000) {Array(9) { IntArray(9) }}
        val content = path.readLines()
        (0 until 1000).forEach { a ->
            val currChunk = content[a].chunked(1)
            (0..80).forEach { r ->
                val col = r/9
                val row = r%9
                sudokuProblemSet[a][col][row] = currChunk[r].toInt()
            }
        }
        return sudokuProblemSet
    }

    // print out given array
    fun print(sudokuBoard: Array<IntArray>, end: Boolean = true) {
        if(end) {
            println("\nThe Solution:\n")
        }else{
            println("\nThe Input:\n")
        }
        for (r in 0 until 9) {
            for (c in 0 until 9) {
                print(sudokuBoard[r][c])
                print(" ")
            }
            print("\n")
        }
    }


    // writes Problem & solution to a file
    private fun toFile(sudokuBoard: Array<IntArray> ?, end: Boolean, extension : String = "default"){
        val sb = StringBuilder()
        var fileName : String = if(end) {
            "src\\sudoku\\out\\Solution_"
        }else{
            "src\\sudoku\\out\\Problem_"
        }
        fileName += "$extension.txt"
        if(sudokuBoard!= null) {
            for (r in 0 until 9) {
                for (c in 0 until 9) {
                    sb.append(sudokuBoard[r][c]).append(" ")
                }
                sb.append("\n")
            }
        }else{
            sb.append("not here")
        }
        File(fileName).writeText(sb.toString())
    }

    private fun check(sudokuBoard: Array<IntArray>, row: Int, col: Int, number: Int): Boolean {

        val offset = sqrt(sudokuBoard.size.toDouble()).toInt()
        val boxCol = col - col % offset
        val boxColEnd = boxCol + offset
        val boxRow = row - row % offset
        val boxRowEnd = boxRow + offset

        // check box
        (boxCol until boxColEnd).forEach {
                c -> (boxRow until boxRowEnd).forEach {
                r -> when (number) { sudokuBoard[r][c] -> { return false } }
            }
        }

        // check row
        sudokuBoard.indices.forEach { c -> when (number) {sudokuBoard[row][c] -> { return false } } }

        // check column
        sudokuBoard.indices.forEach { r -> when (number) { sudokuBoard[r][col] -> { return false } } }

        return true
    }

    private fun solver(sudokuBoard: Array<IntArray>): Boolean {
        val flatList = sudokuBoard.flatMap { it.toList()}
        val row: Int
        val col: Int
        if (!flatList.contains(0)){
            return true
        }else {
            val r = flatList.indexOf(0)
            col = r%9
            row = r/9
        }

        // Go through every empty field and try next number, then check recursively if it is correct.
        (1..9).forEach {
                k -> when {
            check(sudokuBoard, row, col, k) -> {
                sudokuBoard[row][col] = k

                if (solver(sudokuBoard)) {
                    return true
                } else {
                    sudokuBoard[row][col] = 0
                }
            }
        }
        }
        return false
    }



}