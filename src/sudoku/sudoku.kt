package sudoku

import java.io.File


/*
SudokuSolver:
    Can solve one sudoku or a set of sudoku's (set boolean to true)

 Methods:
   solve(saveToFile: Boolean, pathProblem: String, pathSolution: String )
        - saveToFile      --> will save the Solution & Problem to a txt-File
        - pathProblem     --> Path to the problem / problemSet
        - pathSolution    --> Path to a single solution

    identifiers    --> List of Identifier hash for Problem/Solution naming
    solutions     --> Solution HashMap <Identifier:Solution>


 */


// Extension
fun String.readLines(): List<String> = File(this).readLines()

fun main() {

    val sudokuSolver = SudokuSolver()
    // Sudoku's with solutions
    sudokuSolver.solve(true,
        "src\\sudoku\\problems\\sudoku1.txt",
        "src\\sudoku\\solutions\\sol1.txt"
    )
    println("This Problems ID is ${sudokuSolver.identifiers[0]}")

    sudokuSolver.solve(false,
        "src\\sudoku\\problems\\sudoku2.txt",
        "src\\sudoku\\solutions\\sol2.txt"
    )


    // one without Solution
    sudokuSolver.solve(false,
        "src\\sudoku\\problems\\sudoku3.txt"
    )

    sudokuSolver.solve(false,
        "src\\sudoku\\problems\\sudoku4.txt"
    )



    // a set of 1000 Sudoku's
    val sudokuSetSolver = SudokuSolver(true)
    sudokuSetSolver.solve(false,
        "src\\sudoku\\creator\\3.txt"
    )

    // Solutions are stored in HashMap as well and can be found via identifier which is stored in a solutionList
    val solutions = sudokuSetSolver.solutions
    println("ID of Solution 500 : ${sudokuSetSolver.identifiers[500]}\n\nNow printing out that solution:")
    solutions[sudokuSetSolver.identifiers[500]]?.let { sudokuSolver.print(it) }

}